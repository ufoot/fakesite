FROM rust:1-buster

MAINTAINER Christian Mauduit "ufoot@ufoot.org"
LABEL package="fakesite" version="0.1.8" description="fakesite simulates a website with a random behavior. It can be used for load testing web monitors."

ADD ./ /wk/
WORKDIR /wk/
RUN cargo build --release
RUN cp /wk/target/release/fakesite /

# Expose port 80 instead of 8080, running in a docker environment,
# there's usually no port conflict and it's rather the caller who
# will change the port, eg: docker run -p 8080:80 fakesite
# This way, the image can be used, vanilla, in docker environments
# as a drop-in replacement for apache or nginx for testing purposes.

EXPOSE 80
ENTRYPOINT ["/fakesite", ":80"]
