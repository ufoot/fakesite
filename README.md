# Fakesite

[Fakesite](https://gitlab.com/ufoot/fakesite) simulates a website with a random behavior.
It can be used for load testing web monitors.

It is written in [Rust](https://www.rust-lang.org/)
and is mostly a toy project to ramp up on the language.
It might however be useful. Use at your own risk.

![Fakesite icon](https://gitlab.com/ufoot/fakesite/raw/master/fakesite.png)

# Status

[![Build Status](https://gitlab.com/ufoot/fakesite/badges/master/pipeline.svg)](https://gitlab.com/ufoot/fakesite/pipelines)

Current version is 0.1.8.

# Install

No install target yet, copy the `fakesite` binary in your `$PATH` if you wish, that's all.

A few commands which may prove useful:

```sh
cargo build             # build debug binary in ./target/debug/
cargo build --release   # build release binary in ./target/release/
cargo test              # launch tests
rustfmt src/*.rs        # format code
./docker-build.sh       # build Docker image with version tag
./bump-version.sh       # bump minor version number
```

# Usage

Simply launch:

```
cargo run
```

Or just run the binary directly:

```
./target/debug/fakesite
./target/release/fakesite
```

Alternatively, using docker:

```
docker run -p 8080:80 ufoot/fakesite
```

To pass options:

```
cargo run -- :8090
```

By default, the server listens on all interfaces on port 8080.

To change the port the program listens to, pass `:<port>` eg `:8090` to listen on port 8090. Eg:

```
fakesite :8090
```

Other standard options include:

* `-h`, `--help`: display a short help.
* `-v`, `--version`: display version.

# Responses content

The responses, especially the error codes, may look random, but they follow fixed patterns:

* The program switches every 30 seconds from an "OK" to an "ERR" mode. More precisely, every 30 seconds it throws the dices to decide whether to mainly send 200s or 500s. 75% of the time, it is going to choose the "OK" mode. Whenever modes changes, this is reported on STDOUT.
* It logs on STDOUT the requests received using the [Apache common line format](https://httpd.apache.org/docs/1.3/logs.html#common), this allows you to get feedback on what's happening.
* In OK mode, in returns:
  * 50% of 200s
  * 25% of 302s
  * 5% of 401s
  * 5% of 403s
  * 10% of 404s
  * 5% of 500s
* in ERR mode, it returns:
  * 10% of 200s
  * 90% of 500s
* it waits for a random time before serving the answer, this time should generally be below 1 second, but it can occasionnally be higher, around 2 seconds typically.

A typical output from the program could be:

```
# [04/Dec/2019:21:06:46 +0000] start HTTP server listening on 127.0.0.1:8080
127.0.0.1 - - [04/Dec/2019:21:06:46 +0000] "GET / HTTP/1.0" 302 0
127.0.0.1 - - [04/Dec/2019:21:06:47 +0000] "GET / HTTP/1.0" 302 0
127.0.0.1 - - [04/Dec/2019:21:06:48 +0000] "GET / HTTP/1.0" 200 51
127.0.0.1 - - [04/Dec/2019:21:06:49 +0000] "GET / HTTP/1.0" 302 0
127.0.0.1 - - [04/Dec/2019:21:06:49 +0000] "GET / HTTP/1.0" 302 0
127.0.0.1 - - [04/Dec/2019:21:06:50 +0000] "GET / HTTP/1.0" 200 51
127.0.0.1 - - [04/Dec/2019:21:06:51 +0000] "GET / HTTP/1.0" 500 31
127.0.0.1 - - [04/Dec/2019:21:07:14 +0000] "GET / HTTP/1.0" 404 28
127.0.0.1 - - [04/Dec/2019:21:07:15 +0000] "GET / HTTP/1.0" 200 51
127.0.0.1 - - [04/Dec/2019:21:07:16 +0000] "GET / HTTP/1.0" 404 28
# [04/Dec/2019:21:07:16 +0000] switch to ERR mode (lots of 500s)
127.0.0.1 - - [04/Dec/2019:21:07:16 +0000] "GET / HTTP/1.0" 500 31
127.0.0.1 - - [04/Dec/2019:21:07:17 +0000] "GET / HTTP/1.0" 500 31
```

# Legal information

Fakesite is licensed under the [MIT](https://gitlab.com/ufoot/fakesite/blob/master/LICENSE) license.
