//! [Fakesite](https://gitlab.com/ufoot/fakesite) simulates a website with a random behavior.
//! It can be used for load testing web monitors.
//!
//! It is written in [Rust](https://www.rust-lang.org/)
//! and is mostly a toy project to ramp up on the language.
//! It might however be useful. Use at your own risk.
//!
//! ![Fakesite icon](https://gitlab.com/ufoot/fakesite/raw/master/fakesite.png)

pub mod options;
pub mod server;
