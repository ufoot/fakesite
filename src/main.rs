use fakesite::{options, server};

fn main() {
    let cfg = options::parse_options();
    server::start(cfg);
}
