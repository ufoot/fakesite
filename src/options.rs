use std::env;
use std::process::exit;

#[derive(Clone, Copy)]
pub struct Config {
    pub listen_address: &'static str,
    pub http_port: u16,
}

const DEFAULT_LISTEN_ADDRESS: &str = "0.0.0.0";
const DEFAULT_HTTP_PORT: u16 = 8080;

pub const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

pub fn parse_options() -> Config {
    let mut c: Config = Config {
        listen_address: DEFAULT_LISTEN_ADDRESS,
        http_port: DEFAULT_HTTP_PORT,
    };
    let mut first: bool = true;

    for arg in env::args() {
        if first {
            first = false;
            continue;
        }
        match arg.as_str() {
            "-h" | "--help" => {
                help();
                exit(0);
            }
            "-v" | "--version" => {
                version();
                exit(0);
            }
            _ => (),
        }
        if arg.len() > 1 && arg[0..1] == *":" {
            match arg[1..].parse::<u16>() {
                Ok(http_port) => {
                    if http_port > 0 {
                        c.http_port = http_port;
                    } else {
                        println!("invalid http port: {}", http_port);
                        println!();
                        usage();
                        exit(1);
                    }
                    continue;
                }
                Err(e) => {
                    println!("error parsing http port \"{}\": {}", arg, e);
                    println!();
                    usage();
                    exit(1);
                }
            }
        }
        println!("invalid arg: \"{}\"", arg);
        println!();
        usage();
        exit(1);
    }

    return c;
}

fn help() {
    copyright();
    println!();
    usage();
}

fn version() {
    println!("fakesite-{}", VERSION.unwrap_or("unknown"));
}

fn copyright() {
    println!("fakesite is licensed under the MIT license");
    println!("Copyright (C) 2020 Christian Mauduit <ufoot@ufoot.org>");
    println!();
    println!("https://gitlab.com/ufoot/fakesite");
}

fn usage() {
    println!("usage: fakesite [-h] [-v] [:<port>]");
    println!("example: fakesite :8090");
    println!();
    println!("options:");
    println!("-h,--help: display help");
    println!("-v,--version: show version");
    println!(
        ":<port> listen on this TCP port, default {}",
        DEFAULT_HTTP_PORT
    );
}
