extern crate chrono;
extern crate rand;
extern crate rouille;
extern crate ticker;

use chrono::offset::Utc;
use chrono::DateTime;
use rand::Rng;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering::Relaxed;
use std::thread;
use std::time;
use std::time::Duration;
use ticker::Ticker;

use crate::options;

const HTTP_CODES_OK: [u16; 20] = [
    200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 302, 302, 302, 302, 302, 401, 403, 404, 404,
    500,
];
const HTTP_CODES_ERR: [u16; 10] = [200, 500, 500, 500, 500, 500, 500, 500, 500, 500];
const MIN_DELAY_MSEC: u64 = 100;
const MAX_DELAY_MSEC: u64 = 2000;
const OK_FREQ: f64 = 0.75;
const SWITCH_MODE_DELAY_SEC: u64 = 30;

static OK_ERR_MODE: AtomicBool = AtomicBool::new(true);

fn log(request: &rouille::Request, code: u16, size: usize) {
    let dt: DateTime<Utc> = Utc::now();
    let ts = dt.format("%d/%b/%Y:%H:%M:%S %z");
    // here we're logging HTTP/1.0 regardless of the real protocol.
    println!(
        "{} - - [{}] \"{} {} HTTP/1.0\" {} {}",
        request.remote_addr().ip(),
        ts,
        request.method(),
        request.url(),
        code,
        size
    );
}

fn get_code() -> u16 {
    let mut rng = rand::thread_rng();
    let http_codes: &[u16] = if OK_ERR_MODE.load(Relaxed) {
        &HTTP_CODES_OK
    } else {
        &HTTP_CODES_ERR
    };
    return http_codes[rng.gen_range(0, http_codes.len())];
}

fn wait_a_bit() {
    const EXTRA_SCALE: u64 = 100;
    let mut rng = rand::thread_rng();
    let delay = time::Duration::from_millis(
        rng.gen_range(0, EXTRA_SCALE)
            * rng.gen_range(0, EXTRA_SCALE)
            * rng.gen_range(MIN_DELAY_MSEC, MAX_DELAY_MSEC)
            / (EXTRA_SCALE * EXTRA_SCALE),
    );

    thread::sleep(delay);
}

fn update_ok_err_mode() {
    let mut rng = rand::thread_rng();
    let d = Duration::from_secs(SWITCH_MODE_DELAY_SEC);
    for _ in Ticker::new(0.., d).into_iter() {
        let old_ok_err: bool = OK_ERR_MODE.load(Relaxed);
        let new_ok_err: bool = rng.gen_bool(OK_FREQ);
        if old_ok_err != new_ok_err {
            let dt: DateTime<Utc> = Utc::now();
            let ts = dt.format("%d/%b/%Y:%H:%M:%S %z");
            if new_ok_err {
                println!("# [{}] switch to OK mode (lots of 200s)", ts);
            } else {
                println!("# [{}] switch to ERR mode (lots of 500s)", ts);
            }
        }
        OK_ERR_MODE.store(new_ok_err, Relaxed);
    }
}

fn handle(request: &rouille::Request) -> rouille::Response {
    wait_a_bit();
    let code: u16 = get_code();
    let content: String = match code {
        200 => format!(
            "[fakesite] this is the response to request \"{} {}\"\n",
            request.method(),
            request.url()
        ),
        302 => format!(""),
        401 => format!(
            "[fakesite] {} access forbidden to \"{}\"",
            code,
            request.url()
        ),
        403 => format!(
            "[fakesite] {} authorization required for \"{}\"",
            code,
            request.url()
        ),
        404 => format!("[fakesite] {} not found \"{}\"", code, request.url()),
        500 => format!("[fakesite] {} server error \"{}\"", code, request.url()),
        _ => format!("[fakesite] {} ooops \"{}\"", code, request.url()),
    };
    log(&request, code, content.len());
    let response: rouille::Response = match code {
        200 => rouille::Response::text(content),
        302 => rouille::Response::redirect_302("/redirected"),
        401 | 403 | 404 | 500 => rouille::Response::text(content).with_status_code(code),
        _ => {
            panic!(content);
        }
    };
    return response;
}

pub fn start(cfg: options::Config) {
    let dt: DateTime<Utc> = Utc::now();
    let ts = dt.format("%d/%b/%Y:%H:%M:%S %z");
    println!(
        "# [{}] start fakesite HTTP server listening on {}:{} version {}",
        ts,
        cfg.listen_address,
        cfg.http_port,
        options::VERSION.unwrap_or("unknown"),
    );

    let _child = thread::spawn(move || {
        update_ok_err_mode();
    });

    let bind_addr = format!("{}:{}", cfg.listen_address, cfg.http_port);
    rouille::start_server(bind_addr, move |request| handle(request));
}
